import time
import serial
import websocket

# configure the serial connections (the parameters differs on the device you are connecting to)
ser = serial.Serial('/dev/ttyACM0')
ws = websocket.WebSocket()
ws.connect("ws://psjmcneillread.eu-gb.mybluemix.net/ws/location")
print 'Enter your commands below.\r\nInsert "exit" to leave the application.'

input=1
while 1 :
    print "Input"
    # get keyboard input
    result =  ws.recv()
        # Python 3 users
        # input = input(">> ")
    if result == 'exit':
        ser.close()
        exit()
    else:
        # send the character to the device
        # (note that I happend a \r\n carriage return and line feed to the characters - this is requested by my device)
        ser.write(result + '\r\n')
	print "Sent: " + result
        out = ''
        # let's wait one second before reading output (let's give device time to answer)
        #
	
        #while ser.inWaiting() > 0:
        print ser.inWaiting()
	time.sleep(1)
	while(ser.inWaiting()>0):
       		out = ser.readline()
		ws.send(out)
	
	print "Received: " + out
       
