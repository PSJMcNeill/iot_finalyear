import time
import serial
import websocket

# configure the serial connections (the parameters differs on the device you are connecting to)
ser = serial.Serial(
    port='/dev/ttyACM0',
    baudrate=9600,
    parity=serial.PARITY_ODD,
    stopbits=serial.STOPBITS_TWO,
    bytesize=serial.SEVENBITS
)

#ws = websocket.WebSocket();
#ws.connect("ws://psjmcneillread.eu-gb.mybluemix.net/ws/location")
#print 'Websocket Connected'

print 'Enter your commands below.\r\nInsert "exit" to leave the application.'

input=1
while 1 :
    #print 'Awaiting input'
    #result =  #ws.recv()
    #print result
    # get keyboard input
        # Python 3 users
    input = raw_input(">> ")
    if input == 'exit':
	print 'Exiting'
        ser.close()
        exit()
    else:
        # send the character to the device
        # (note that I happend a \r\n carriage return and line feed to the characters - this is requested by my device)
	#print result
        ser.write(input + '\r\n')
        out = ''
        # let's wait one second before reading output (let's give device time to answer)
        time.sleep(1)
        while ser.inWaiting() > 0:
        	out += ser.read(1)
		print "Returned: " + out
        if out != '':
	    #ws.send('Message returned')
            print "Received: " + out
