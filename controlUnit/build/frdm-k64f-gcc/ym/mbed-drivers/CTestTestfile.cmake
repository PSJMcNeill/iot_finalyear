# CMake generated Testfile for 
# Source directory: /home/boogle/Final Year/controlUnit/build/frdm-k64f-gcc/ym/mbed-drivers
# Build directory: /home/boogle/Final Year/controlUnit/build/frdm-k64f-gcc/ym/mbed-drivers
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(../mbed-hal)
subdirs(../cmsis-core)
subdirs(../ualloc)
subdirs(../minar)
subdirs(../core-util)
subdirs(../compiler-polyfill)
subdirs(../greentea-client)
subdirs(source)
subdirs(test)
