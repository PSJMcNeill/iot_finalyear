# NOTE: This file is generated by yotta: changes will be overwritten!


project(mbed-hal-ksdk-mcu)


# include root directories of all components we depend on (directly and
# indirectly, including ourself)
include_directories("/home/boogle/Final Year/controlUnit")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-ksdk-mcu")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/uvisor-lib")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-k64f")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/cmsis-core")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/cmsis-core-freescale")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/cmsis-core-k64f")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-drivers")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-freescale")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/ualloc")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/minar")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/core-util")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/compiler-polyfill")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/greentea-client")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/dlmalloc")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/minar-platform")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/minar-platform-mbed")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-ksdk-mcu")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-frdm-k64f")


# recurse into dependencies that aren't built elsewhere
add_subdirectory(
   "/home/boogle/Final Year/controlUnit/build/frdm-k64f-gcc/ym/uvisor-lib"
   "${CMAKE_BINARY_DIR}/ym/uvisor-lib"
)
add_subdirectory(
   "/home/boogle/Final Year/controlUnit/build/frdm-k64f-gcc/ym/mbed-hal-k64f"
   "${CMAKE_BINARY_DIR}/ym/mbed-hal-k64f"
)



# And others (typically CMSIS implementations) need to export non-system header
# files. Please don't use this facility. Please. It's much, much better to fix
# implementations that import these headers to import them using the full path.
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-ksdk-mcu/mbed-hal-ksdk-mcu")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-ksdk-mcu/mbed-hal-ksdk-mcu/TARGET_KSDK_CODE/common/phyksz8081")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-ksdk-mcu/mbed-hal-ksdk-mcu/TARGET_KSDK_CODE/drivers/clock")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-ksdk-mcu/mbed-hal-ksdk-mcu/TARGET_KSDK_CODE/drivers/enet")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-ksdk-mcu/mbed-hal-ksdk-mcu/TARGET_KSDK_CODE/drivers/interrupt")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-ksdk-mcu/mbed-hal-ksdk-mcu/TARGET_KSDK_CODE/drivers/pit")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-ksdk-mcu/mbed-hal-ksdk-mcu/TARGET_KSDK_CODE/drivers/pit/common")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-ksdk-mcu/mbed-hal-ksdk-mcu/TARGET_KSDK_CODE/hal/adc")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-ksdk-mcu/mbed-hal-ksdk-mcu/TARGET_KSDK_CODE/hal/can")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-ksdk-mcu/mbed-hal-ksdk-mcu/TARGET_KSDK_CODE/hal/dac")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-ksdk-mcu/mbed-hal-ksdk-mcu/TARGET_KSDK_CODE/hal/dmamux")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-ksdk-mcu/mbed-hal-ksdk-mcu/TARGET_KSDK_CODE/hal/dspi")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-ksdk-mcu/mbed-hal-ksdk-mcu/TARGET_KSDK_CODE/hal/edma")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-ksdk-mcu/mbed-hal-ksdk-mcu/TARGET_KSDK_CODE/hal/enet")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-ksdk-mcu/mbed-hal-ksdk-mcu/TARGET_KSDK_CODE/hal/flextimer")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-ksdk-mcu/mbed-hal-ksdk-mcu/TARGET_KSDK_CODE/hal/gpio")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-ksdk-mcu/mbed-hal-ksdk-mcu/TARGET_KSDK_CODE/hal/i2c")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-ksdk-mcu/mbed-hal-ksdk-mcu/TARGET_KSDK_CODE/hal/llwu")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-ksdk-mcu/mbed-hal-ksdk-mcu/TARGET_KSDK_CODE/hal/lptmr")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-ksdk-mcu/mbed-hal-ksdk-mcu/TARGET_KSDK_CODE/hal/lpuart")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-ksdk-mcu/mbed-hal-ksdk-mcu/TARGET_KSDK_CODE/hal/mcg")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-ksdk-mcu/mbed-hal-ksdk-mcu/TARGET_KSDK_CODE/hal/mpu")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-ksdk-mcu/mbed-hal-ksdk-mcu/TARGET_KSDK_CODE/hal/osc")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-ksdk-mcu/mbed-hal-ksdk-mcu/TARGET_KSDK_CODE/hal/pdb")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-ksdk-mcu/mbed-hal-ksdk-mcu/TARGET_KSDK_CODE/hal/pit")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-ksdk-mcu/mbed-hal-ksdk-mcu/TARGET_KSDK_CODE/hal/pmc")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-ksdk-mcu/mbed-hal-ksdk-mcu/TARGET_KSDK_CODE/hal/port")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-ksdk-mcu/mbed-hal-ksdk-mcu/TARGET_KSDK_CODE/hal/rcm")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-ksdk-mcu/mbed-hal-ksdk-mcu/TARGET_KSDK_CODE/hal/rtc")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-ksdk-mcu/mbed-hal-ksdk-mcu/TARGET_KSDK_CODE/hal/sai")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-ksdk-mcu/mbed-hal-ksdk-mcu/TARGET_KSDK_CODE/hal/sdhc")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-ksdk-mcu/mbed-hal-ksdk-mcu/TARGET_KSDK_CODE/hal/sim")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-ksdk-mcu/mbed-hal-ksdk-mcu/TARGET_KSDK_CODE/hal/smc")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-ksdk-mcu/mbed-hal-ksdk-mcu/TARGET_KSDK_CODE/hal/uart")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-ksdk-mcu/mbed-hal-ksdk-mcu/TARGET_KSDK_CODE/hal/wdog")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-ksdk-mcu/mbed-hal-ksdk-mcu/TARGET_KSDK_CODE/utilities")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal/mbed-hal")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-k64f/mbed-hal-k64f")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-k64f/mbed-hal-k64f/device")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-k64f/mbed-hal-k64f/MK64F12")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/cmsis-core/cmsis-core")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/cmsis-core-k64f/cmsis-core-k64f")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-drivers/mbed")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-ksdk-mcu/mbed-hal-ksdk-mcu")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-ksdk-mcu/mbed-hal-ksdk-mcu/TARGET_KSDK_CODE/common/phyksz8081")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-ksdk-mcu/mbed-hal-ksdk-mcu/TARGET_KSDK_CODE/drivers/clock")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-ksdk-mcu/mbed-hal-ksdk-mcu/TARGET_KSDK_CODE/drivers/enet")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-ksdk-mcu/mbed-hal-ksdk-mcu/TARGET_KSDK_CODE/drivers/interrupt")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-ksdk-mcu/mbed-hal-ksdk-mcu/TARGET_KSDK_CODE/drivers/pit")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-ksdk-mcu/mbed-hal-ksdk-mcu/TARGET_KSDK_CODE/drivers/pit/common")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-ksdk-mcu/mbed-hal-ksdk-mcu/TARGET_KSDK_CODE/hal/adc")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-ksdk-mcu/mbed-hal-ksdk-mcu/TARGET_KSDK_CODE/hal/can")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-ksdk-mcu/mbed-hal-ksdk-mcu/TARGET_KSDK_CODE/hal/dac")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-ksdk-mcu/mbed-hal-ksdk-mcu/TARGET_KSDK_CODE/hal/dmamux")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-ksdk-mcu/mbed-hal-ksdk-mcu/TARGET_KSDK_CODE/hal/dspi")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-ksdk-mcu/mbed-hal-ksdk-mcu/TARGET_KSDK_CODE/hal/edma")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-ksdk-mcu/mbed-hal-ksdk-mcu/TARGET_KSDK_CODE/hal/enet")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-ksdk-mcu/mbed-hal-ksdk-mcu/TARGET_KSDK_CODE/hal/flextimer")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-ksdk-mcu/mbed-hal-ksdk-mcu/TARGET_KSDK_CODE/hal/gpio")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-ksdk-mcu/mbed-hal-ksdk-mcu/TARGET_KSDK_CODE/hal/i2c")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-ksdk-mcu/mbed-hal-ksdk-mcu/TARGET_KSDK_CODE/hal/llwu")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-ksdk-mcu/mbed-hal-ksdk-mcu/TARGET_KSDK_CODE/hal/lptmr")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-ksdk-mcu/mbed-hal-ksdk-mcu/TARGET_KSDK_CODE/hal/lpuart")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-ksdk-mcu/mbed-hal-ksdk-mcu/TARGET_KSDK_CODE/hal/mcg")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-ksdk-mcu/mbed-hal-ksdk-mcu/TARGET_KSDK_CODE/hal/mpu")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-ksdk-mcu/mbed-hal-ksdk-mcu/TARGET_KSDK_CODE/hal/osc")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-ksdk-mcu/mbed-hal-ksdk-mcu/TARGET_KSDK_CODE/hal/pdb")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-ksdk-mcu/mbed-hal-ksdk-mcu/TARGET_KSDK_CODE/hal/pit")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-ksdk-mcu/mbed-hal-ksdk-mcu/TARGET_KSDK_CODE/hal/pmc")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-ksdk-mcu/mbed-hal-ksdk-mcu/TARGET_KSDK_CODE/hal/port")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-ksdk-mcu/mbed-hal-ksdk-mcu/TARGET_KSDK_CODE/hal/rcm")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-ksdk-mcu/mbed-hal-ksdk-mcu/TARGET_KSDK_CODE/hal/rtc")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-ksdk-mcu/mbed-hal-ksdk-mcu/TARGET_KSDK_CODE/hal/sai")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-ksdk-mcu/mbed-hal-ksdk-mcu/TARGET_KSDK_CODE/hal/sdhc")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-ksdk-mcu/mbed-hal-ksdk-mcu/TARGET_KSDK_CODE/hal/sim")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-ksdk-mcu/mbed-hal-ksdk-mcu/TARGET_KSDK_CODE/hal/smc")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-ksdk-mcu/mbed-hal-ksdk-mcu/TARGET_KSDK_CODE/hal/uart")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-ksdk-mcu/mbed-hal-ksdk-mcu/TARGET_KSDK_CODE/hal/wdog")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-ksdk-mcu/mbed-hal-ksdk-mcu/TARGET_KSDK_CODE/utilities")
include_directories("/home/boogle/Final Year/controlUnit/yotta_modules/mbed-hal-frdm-k64f/mbed-hal-frdm-k64f")


# modules with custom CMake build systems may append to the
# YOTTA_GLOBAL_INCLUDE_DIRS property to add compile-time-determined include
# directories:
get_property(GLOBAL_INCLUDE_DIRS GLOBAL PROPERTY YOTTA_GLOBAL_INCLUDE_DIRS)
include_directories(${GLOBAL_INCLUDE_DIRS})

# Provide versions of all the components we depend on, the corresponding
# preprocessor definitions are generated in yotta_config.h
set(YOTTA_UVISOR_LIB_VERSION_STRING "1.0.13")
set(YOTTA_UVISOR_LIB_VERSION_MAJOR 1)
set(YOTTA_UVISOR_LIB_VERSION_MINOR 0)
set(YOTTA_UVISOR_LIB_VERSION_PATCH 13)
set(YOTTA_MBED_HAL_K64F_VERSION_STRING "1.1.0")
set(YOTTA_MBED_HAL_K64F_VERSION_MAJOR 1)
set(YOTTA_MBED_HAL_K64F_VERSION_MINOR 1)
set(YOTTA_MBED_HAL_K64F_VERSION_PATCH 0)
set(YOTTA_MBED_HAL_KSDK_MCU_VERSION_STRING "1.0.9")
set(YOTTA_MBED_HAL_KSDK_MCU_VERSION_MAJOR 1)
set(YOTTA_MBED_HAL_KSDK_MCU_VERSION_MINOR 0)
set(YOTTA_MBED_HAL_KSDK_MCU_VERSION_PATCH 9)

# recurse into subdirectories for this component, using the two-argument
# add_subdirectory because the directories referred to here exist in the source
# tree, not the working directory
add_subdirectory(
    "/home/boogle/Final Year/controlUnit/build/frdm-k64f-gcc/ym/mbed-hal-ksdk-mcu/source"
    "${CMAKE_BINARY_DIR}/ym/mbed-hal-ksdk-mcu/source"
)
