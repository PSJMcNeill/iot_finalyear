#ifndef __YOTTA_BUILD_INFO_H__
#define __YOTTA_BUILD_INFO_H__
// yotta build info, #include YOTTA_BUILD_INFO_HEADER to access
#define YOTTA_BUILD_YEAR 2016 // UTC year
#define YOTTA_BUILD_MONTH 3 // UTC month 1-12
#define YOTTA_BUILD_DAY 7 // UTC day 1-31
#define YOTTA_BUILD_HOUR 22 // UTC hour 0-24
#define YOTTA_BUILD_MINUTE 36 // UTC minute 0-59
#define YOTTA_BUILD_SECOND 52 // UTC second 0-61
#define YOTTA_BUILD_UUID 1ba18330-7b41-41e1-a37a-146b78a4e1ab // unique random UUID for each build
#endif // ndef __YOTTA_BUILD_INFO_H__
