#include "mbed-drivers/mbed.h"
#include "NRF2401P.h"
Serial pc(USBTX, USBRX);
DigitalOut gpo(D0);

NRF2401P wireless(PTD6,PTD7, PTD5,PTD4, PTB20);
DigitalOut led(LED1);

DigitalIn but1(SW2);
DigitalIn but2(SW3);



long long addr1=0xAB00CD;
int channel =0x12;
char msg[32];
char len;

static void sendData(void){
	wait(1);
	if(pc.readable()){
		char c = pc.getc();
		wireless.quickTxSetup(channel, addr1);

		if((c == 'w')) {    
                    
                    led = 1;
                    strcpy (msg, "w");
                    wireless.transmitData(msg ,strlen(msg));
                    pc.printf("W \n");
                    }
           	else if((c == 's')){
                    //pc.printf("Back \n");
                    strcpy (msg, "ss");
                    wireless.transmitData(msg ,strlen(msg));
                    }
           	else if((c == 'd')){
                    //pc.printf("Right \n");
                    strcpy (msg, "ddd");
                    wireless.transmitData(msg ,strlen(msg));
                    }
             	else if((c == 'a')){
                    //pc.printf("Left \n");
                    strcpy (msg, "aaaa");
                    wireless.transmitData(msg ,strlen(msg));
                }
}
}


static void listen(void){
	wireless.quickRxSetup(channel, addr1);
	wait(1);
	if(wireless.isRxData()){
        	pc.printf("%s\r\n", msg);
    	}
    	else{
        	pc.printf("Nothing Received \r\n");
    	}

}




void app_start(int, char**) {
    minar::Scheduler::postCallback(listen).period(minar::milliseconds(500));
    minar::Scheduler::postCallback(sendData).period(minar::milliseconds(500));
}

