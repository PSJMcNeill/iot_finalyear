# greentea-client

**Work in progress!**
NOTE: **Please do not use this package** before we release it to yotta registry! Thanks! <3

Extraction of implementation of:
* ```mbed-drivers``` [test_env.cpp](https://github.com/PrzemekWirkus/mbed-drivers/blob/master/source/test_env.cpp) and
* ```mbed-drivers``` [test_env.h](https://github.com/PrzemekWirkus/mbed-drivers/blob/master/mbed-drivers/test_env.h).

See [greentea](https://github.com/ARMmbed/mbed-drivers/pull/149) and latest changes regarding DUT-host transport layer.

## yotta registry
You can find us here [greentea-client v0.1.4](https://yotta.mbed.com/#/module/greentea-client/0.1.4).
