// Sweep the motor speed from full-speed reverse (-1.0) to full speed forwards (1.0)

#include "mbed.h"
#include "Motor.h"
#include "NRF2401P.h"

void moveForward(void);
void moveBackward(void);
void turnLeft(void);
void turnRight(void);
void listen(void);

Motor m(D11, D12, D13); // pwm, fwd, rev
Motor n(D10,D7,D6);
NRF2401P wireless(PTD6,PTD7, PTD5,PTD4, PTB20);
long long addr1=0xAB00CD; // setup address - any 5 byte number - same as TX
  int channel =0x12;  // [0-126] setup channel, must be same as TX
  bool txOK;
  char msg[32];
  char ackData[32];
  char len;
  bool forward = false;
  bool backward = false;
  bool right = false;
  bool left =  false;




int main() {  
  printf("Main Initiaited \r\n");
  while(1){
  listen();
  }
}

void listen(void){
    printf("Begining Listening \r\n");
    wireless.quickRxSetup(channel, addr1); 
   printf("Setup Complete \r\n"); 
  //if(wireless.isRxData()){
  // set ack data
  sprintf(ackData,"Ack data \r\n");
  wireless.acknowledgeData(ackData, strlen(ackData),1); // ack for pipe 1  
  // receive
  printf("Begin Receiving \r\n");
  while (! wireless.isRxData()); // note this blocks until RX data
  len= wireless.getRxData(msg); // gets the message, len is length of msg
  printf("Message Received: %s \r\n", msg);
  if(len == 1){
  
    m.speed(1);
    n.speed(1);
  
    wait(0.5);
    m.speed(0);
    n.speed(0);
  
  }
  else if(len == 2) {
   
    m.speed(-1);
    n.speed(-1);

  else if(len == 3) {
   
    m.speed(1);
    n.speed(0);

    wait(0.2);
    m.speed(0);
    n.speed(0);
 

    }  
  else if(len == 4){

    m.speed(0);
    n.speed(1);
 
    wait(0.2);
    m.speed(0);
    n.speed(0);
   
    }  
  }

