set(CMAKE_HOST_SYSTEM "Linux-3.19.0-25-generic")
set(CMAKE_HOST_SYSTEM_NAME "Linux")
set(CMAKE_HOST_SYSTEM_VERSION "3.19.0-25-generic")
set(CMAKE_HOST_SYSTEM_PROCESSOR "x86_64")

include("/home/boogle/Final Year/robotCode/build/frdm-k64f-gcc/toolchain.cmake")

set(CMAKE_SYSTEM "mbedOS-1")
set(CMAKE_SYSTEM_NAME "mbedOS")
set(CMAKE_SYSTEM_VERSION "1")
set(CMAKE_SYSTEM_PROCESSOR "armv7-m")

set(CMAKE_CROSSCOMPILING "TRUE")

set(CMAKE_SYSTEM_LOADED 1)
