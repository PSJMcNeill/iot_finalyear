#ifndef __YOTTA_BUILD_INFO_H__
#define __YOTTA_BUILD_INFO_H__
// yotta build info, #include YOTTA_BUILD_INFO_HEADER to access
#define YOTTA_BUILD_YEAR 2016 // UTC year
#define YOTTA_BUILD_MONTH 4 // UTC month 1-12
#define YOTTA_BUILD_DAY 13 // UTC day 1-31
#define YOTTA_BUILD_HOUR 13 // UTC hour 0-24
#define YOTTA_BUILD_MINUTE 4 // UTC minute 0-59
#define YOTTA_BUILD_SECOND 36 // UTC second 0-61
#define YOTTA_BUILD_UUID bcf374dd-5b90-406b-b2af-d47c7959f8be // unique random UUID for each build
#endif // ndef __YOTTA_BUILD_INFO_H__
