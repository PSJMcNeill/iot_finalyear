#include "mbed-drivers/mbed.h"
#include "Motor.h"
#include "NRF2401P.h"
#include "FXOS8700Q.h"

//Declaration allows Scheduler to be used for callbacks
using minar::Scheduler;

//Motors are declared in the format (pwm, fwd, rev)
Motor m(D6, D11, D12);
Motor n(D8,D10,D9);

//Declaration of Serial Connection
Serial pc(USBTX, USBRX);
//Declaration of Acceleromter
FXOS8700QAccelerometer acc(i2c, FXOS8700CQ_SLAVE_ADDR1);

//Variables used for frequency of wireless chip
long long addr1=0xAB00CD; 
int channel =0x12;  
bool txOK;
int xValue= 0;
int yValue= 0;
int zValue= 0;
char msg[64];
char xData[64];
char yData[64];
char zData[64];
char accelData[64];
char ackData[32];
char len;
DigitalOut led(LED1);

float faX, faY, faZ, tmp_float;
int16_t raX, raY, raZ, tmp_int;
motion_data_units_t acc_data;

NRF2401P wireless (PTD6, PTD7, PTD5, PTD4, PTB20);

void accel(void){	
	acc.enable();
	acc.getAxis(acc_data);
	xValue = acc.getX(raX);	
	yValue = acc.getY(raY);
	zValue = acc.getZ(raZ);
	sprintf(accelData, "X:%d: Y:%d: Z:%d", xValue, yValue, zValue);
	pc.printf("Data: %s\r\n", accelData);
}


static void listen(void){
	led = 1;
	wireless.quickRxSetup(channel, addr1);
	sprintf(ackData, accelData);
  	wireless.acknowledgeData(ackData, strlen(ackData),0);
	wait(1);
	if(!(wireless.isRxData())){
		Scheduler::postCallback(accel);
	} else{
	len = wireless.getRxData(msg);
	if(len == 1){
		m.speed(0.65);
    		n.speed(0.65);	
  	}
  	else if(len == 2) {
   		m.speed(-0.65);
		n.speed(-0.65);
	}
  	else if(len == 4) {
		m.speed(0);
		n.speed(0.5);
    			
    	}  
  	else if(len == 3){
   		m.speed(0.5);
		n.speed(0);	
    	}  
	else if(len == 5){
		m.speed(0);
		n.speed(0);	
	}
	}
	led = 0;
	len = 0;
			
}

void app_start(int, char**) 
{
    Scheduler::postCallback(listen).period(minar::milliseconds(500));    	
}






