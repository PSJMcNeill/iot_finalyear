#include "mbed-drivers/mbed.h"
#include "Motor.h"
#include "NRF2401P.h"

Motor m(D11, D12, D13); // pwm, fwd, rev
Motor n(D10,D7,D6);

Serial pc(USBTX, USBRX);
long long addr1=0xAB00CD; // setup address - any 5 byte number - same as RX
int channel =0x12;  // [0-126] setup channel, must be same as RX
bool txOK;
char msg[32];
char accelData[64];
char ackData[32];
char test[32];
char len;
DigitalOut led(LED1);
DigitalOut led1(LED2);

	

NRF2401P wireless (PTD6, PTD7, PTD5, PTD4, PTB20);

static void listen(void){
	pc.printf("Listen\n");
	led = 0;
	led1 = 0;
	wireless.quickRxSetup(channel, addr1);
	sprintf(ackData,"Ack data \r\n");
  	wireless.acknowledgeData(ackData, strlen(ackData),1);
	//wait(1);
	//if(wireless.isRxData()){
	while(!(wireless.isRxData())){
		listen.detach();
	}	
	len = wireless.getRxData(msg);	
	//}else{
	//len = 0;
	//}	
}


static void motor(void){	
	pc.printf("Motor\n");
	led = 0;
	led1 = 0;
	if(len == 1){
    		m.speed(1);
    		n.speed(1);
		strcpy(accelData, "Forward");
  		led1 = 1;
   		 wait(0.5);
   		 m.speed(0);
    		n.speed(0);
  	}
  	else if(len == 2) {
   	m.speed(-1);
   	n.speed(-1);
	strcpy(accelData, "Backward");
	led1 = 1;
	wait(0.5);
	m.speed(-1);
   	n.speed(-1);
	}
  	else if(len == 3) {
    	m.speed(1);
    	n.speed(0);
	strcpy(accelData, "Left");
	led1 = 1;
   	wait(0.2);
    	m.speed(0);
    	n.speed(0);
    	}  
  	else if(len == 4){
   	m.speed(0);
    	n.speed(1);
	strcpy(accelData, "Right");
	led1 = 1;
    	wait(0.2);
    	m.speed(0);
    	n.speed(0);
    	}  

	led = 0;
	led1 = 0;
	
}

static void sendData(){
	pc.printf("Send \n");
	wireless.quickTxSetup(channel, addr1);
	strcpy(accelData, "Robot");
	wireless.transmitData(accelData ,strlen(accelData));
	pc.printf(accelData);
	led = 0;
	led1 = 0;

}

void app_start(int, char**) {
    minar::Scheduler::postCallback(listen).period(minar::milliseconds(1500));	
    minar::Scheduler::postCallback(motor).period(minar::milliseconds(1000));
    minar::Scheduler::postCallback(sendData).period(minar::milliseconds(1000)).delay(minar::milliseconds(3000));
}










