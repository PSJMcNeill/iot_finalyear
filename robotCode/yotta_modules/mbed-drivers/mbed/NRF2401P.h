/**
 *@section DESCRIPTION
 * mbed NRF2401+  Library
 *@section LICENSE
 * Copyright (c) 2015, Malcolm McCulloch
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * @file "NRF2401P.h"
 */

#ifndef MBED_NRF2401P_H
#define MBED_NRF2401P_H
 
/** \brief A library to drive the nRF24l01+
* 
*   This library is a simple way to get the nRF24l01+ up and running.
*
*- Use case: For a simple transmitter 
*@code
*int main() {
*
*  long long addr1=0xAB00CD; // setup address - any 5 byte number - same as RX
*  int channel =0x12;  // [0-126] setup channel, must be same as RX
*  bool txOK;
*  char msg[32];
*  char ackData[32];
*  char len;
*
*  // Setup 
*  NRF2401P nrf1(PTD6,PTD7, PTD5,PTD4, PTC12); //mosi, miso, sclk, csn, ce)
*  nrf1.quickTxSetup(channel, addr1); // sets nrf24l01+ as transmitter
*
*  // transmit
*  strcpy (msg, "Hello"); 
*  txOK= nrf1.transmitData(msg,strlen(msg));
*
*  // read ack data if available
*  if (nrf1.isAckData()) { 
*      len= nrf1.getRxData(ackData); // len is number of bytes in ackData
*   }
*}
*@endcode
*- Use case: For a simple receiver 
*@code
*int main(){
*        
*  long long addr1=0xAB00CD; // setup address - any 5 byte number - same as TX
*  int channel =0x12;  // [0-126] setup channel, must be same as TX
*  bool txOK;
*  char msg[32];
*  char ackData[32];
*  char len;
*
*  // Setup 
*  NRF2401P nrf1(PTD6,PTD7, PTD5,PTD4, PTC12); //mosi, miso, sclk, csn, ce)
*  nrf1.quickRxSetup(channel, addr1); // sets nrf24l01+ as  receiver, using pipe 1
*
*  // set ack data
*  sprintf(ackData,"Ack data");
*  nrf1.acknowledgeData(ackData, strlen(ackData),1); // ack for pipe 1
*    
*  // receive
*  while (! nrf1.isRxData()); // note this blocks until RX data
*  len= nrf1.getRxData(msg); // gets the message, len is length of msg
*
*}
*@endcode
*/

class NRF2401P
{
public:
    SPI *spi;
    DigitalOut csn,ce;
    char addressWidth;
    char logMsg[80];
    char status;
    char statusS[32];
    char pipe0Add[5];
    char txAdd[5];
    bool dynamic,debug;
    Serial *pc;

    /** Create a radio device for communicating with the RF24L01P via SPI.
     *
     * @param mosi The SPI slave data input pin.
     * @param miso The SPI Slave Data Output (with tri-state option).
     * @param sclk The SPI clock pin.
     * @param _csn The SPI chip select pin.
     * @param _ce Chip Enable pin (Activates RX or TX mode).
     */
    NRF2401P (PinName mosi, PinName miso, PinName sclk, PinName _csn, PinName _ce);

    char acknowledgeData(char *data, char width, char pipe);
    char checkStatus();
    void clearStatus();
    void flushRx();
    void flushTx();
    char getRxData(char * buffer);

    /**  Get the payload width for the next FIFO item (can be dynamic or static payload width)
      *
      * @returns 
      *   width of next available payload.
      */    
    char getRxWidth();

    bool isAckData();
    bool isRxData();
    
    /**  Is the Received Power Detector (ala carrier detect) register set high?
      *
      * @returns 
      *   true on received power levels above -64 dBm present in the RF channel.
      *   false received power is less than -64 dBm (could indicate interference, etc).
      */    
    bool isRPDset();
    
    
    /** Writes message and waits for 10 ms (
      *
      * @param msg 
      */
    void log (char *msg);

    void quickRxSetup(int channel,long long addr);
    void quickTxSetup(int channel,long long addr);
    void readReg(char address, char *data);
    void readReg(char address, char *data, char width);
    char testReceive();
    char testTransmit();
    char transmitData( char *data, char width );

    char setAddressWidth(char width);
    
    /**  Sets the frequency channel nRF24L01+ operates on
     *
     * @param chan F0 = 2400 + chan [MHz]
     * @returns
     *   0 on success,
     *   non-zero on failure.
     */    
    char setChannel(char chan);
    
    void setDynamicPayload();
    char setPwrUp();
    
    /**  Set RF data rate and RF output power in TX mode
     *
     * @param speed RF data rate: '0x00' – 1Mbps, '0x01' – 2Mbps, '0x02' – 250kbps, '0x03' – Reserved
     * @param power RF output power: '0x00' – -18dBm, '0x01' – -12dBm, '0x02' – -6dBm, '0x03' – 0dBm
     * @returns
     *   0 on success,
     *   non-zero on failure.
     */
    char setRadio(char speed, char power);
    
    char setRxAddress(char *address, char pipe);
    char setRxAddress(long long address, char pipe);
    char setRxMode();
    char setTxAddress(char *address);
    char setTxAddress(long long address);
    char setTxMode();
    
    /** Sets the timing and number of TX retries
     *
     * @param delay Auto Retransmit Delay: '0000' – Wait 250uS', '0001' – Wait 500uS, ..., '1111' – Wait 4000uS
     * @param numTries Auto Retransmit Count: '0000' – Re-Transmit disabled, '0001' – Up to 1 Re-Transmit on fail of AA, ..., '1111' – Up to 15 Re-Transmit on fail of AA
     * @returns
     *   0 on success,
     *   non-zero on failure.
     */    
    char setTxRetry(char delay, char numTries);
    
    void start();
    char* statusString();

    /** Print out 1 byte content of register
     *
     * @param name Name of register to print.
     * @param address Register address of interest.
     * @param newline By default print a trailing newline, can disable by setting false.
     */        
    void printReg(char* name, char address, bool newline=true);
    
    /** Print out 'width' bytes content of register
     *
     * @param name Name of register to print.
     * @param address Register address of interest.
     * @param width Number of bytes to print.
     * @param newline By default print a trailing newline, can disable by setting false.
     */        
    void printReg(char* name, char address, char width, bool newline=true);

    /** Print verbose details about the radio setup and configuration
     *
     */    
    void printDetails();

    void writeReg(char address, char *data, char width);
    void writeReg(char address, char data);

    void scratch();


};
#endif